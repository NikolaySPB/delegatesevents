﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DelegatesEvents
{
    public static class Ext
    {
        public static T GetMax<T>(this IEnumerable<T> collection, Func<T, T, T> getParametr) where T : class
        {
            return collection.Aggregate(getParametr);
        }
    }
}
