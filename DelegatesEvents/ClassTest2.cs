﻿using System;
using System.Collections.Generic;

namespace DelegatesEvents
{
    public class ClassTest2
    {
        public event EventHandler<FileArgs> FileFound;

        public void ListDirectory(List<string> list)
        {
            FileArgs fileArgs = new FileArgs();

            foreach (var item in list)
            {
                fileArgs.Message(item);
                OnFileFound(fileArgs);
                SuggestQuitSearch();
            }            
        }

        private void SuggestQuitSearch()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Для отмены поиска нажмите 'Esc', для продолжения 'Enter'");
            Console.ResetColor();

            ConsoleKeyInfo info = Console.ReadKey();
            if (info.Key == ConsoleKey.Escape)
                FileFound += C_FileFound;
        }
        
        private void C_FileFound(object sender, FileArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("The end");
            Console.ReadLine();
            Environment.Exit(0);
        }

        protected virtual void OnFileFound(FileArgs e)
        {
            FileFound?.Invoke(this, e);
        }
    }
}
