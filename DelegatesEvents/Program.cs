﻿using System;
using System.Collections.Generic;

namespace DelegatesEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ClassTest> classTesxes = new List<ClassTest>()
            {
                new ClassTest() { I = 11},
                new ClassTest() { I = 71},
                new ClassTest() { I = 20},
                new ClassTest() { I = 25},
                new ClassTest() { I = 13}
            };

            var max = classTesxes.GetMax((x1, x2) => x1.I > x2.I ? x1 : x2);

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write($"Максимальное значение 'I', среди коллекции классов 'ClassTest': ");
            Console.ResetColor();
            Console.WriteLine(max + "\n");

            List<string> files = new List<string>();

            for (int i = 1; i < 101; i++)
                files.Add($"File{i}.txt");
            
            ClassTest2 c = new ClassTest2();
            c.ListDirectory(files);    
        }
    }
}
