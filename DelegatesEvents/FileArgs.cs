﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesEvents
{
    public class FileArgs : EventArgs
    {
        public string Name = "Найден новый файл: ";

        public void Message(string name)
        {
            Console.WriteLine($"{Name} {name}");
        }
    }
}
