﻿namespace DelegatesEvents
{
    public class ClassTest
    {
        public int I { get; set; }

        public override string ToString()
        {
            return $"{I}";
        }
    }
}
